<?php

get_header();
get_template_part("template-parts/page", "header");
get_template_part("template-parts/page", "profil");
get_template_part("template-parts/page", "posts");
get_template_part("template-parts/page", "reviews");
get_template_part("template-parts/page", "newsletter");
get_sidebar();
get_footer();
